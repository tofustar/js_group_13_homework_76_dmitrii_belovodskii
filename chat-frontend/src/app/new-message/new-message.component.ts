import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessageService } from '../shared/message.service';
import { MessageData } from '../shared/message.model';

@Component({
  selector: 'app-new-message',
  templateUrl: './new-message.component.html',
  styleUrls: ['./new-message.component.sass']
})
export class NewMessageComponent implements OnInit {

  @ViewChild('f') form!: NgForm;

  constructor(private messageService: MessageService) {}

  ngOnInit(): void {
  }

  onSubmit() {
    const msgData: MessageData = this.form.value;
    console.log(this.form.controls);
    this.messageService.createMessage(msgData).subscribe();
  }
}
