import { Injectable } from '@angular/core';
import { map, Subject } from 'rxjs';
import { Message, MessageData } from './message.model';
import { HttpClient } from '@angular/common/http';

@Injectable()

export class MessageService {
  messagesChange = new Subject<Message[]>();
  messagesFetching = new Subject<boolean>();

  private messages: Message[] = [];
  date = '';

  constructor(private http: HttpClient) {
  }


  fetchMsg() {
    this.messagesFetching.next(true);
    return this.http.get<{ [key: string]: Message }>(`http://localhost:8000/messages?datetime=${this.date}`)
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const msgData = result[id];
          return new Message(msgData.id, msgData.author, msgData.message, msgData.datetime);
        });
      }))
      .subscribe(messages => {
        if (messages.length !== 0) {
          this.messages = this.messages.concat(messages);
          this.date = messages[messages.length - 1].datetime;
          this.messagesChange.next(this.messages.slice());
          this.messagesFetching.next(false);
        } else {
          this.messages = messages;
          this.date = messages[messages.length - 1].datetime;
          this.messagesChange.next(this.messages.slice());
          this.messagesFetching.next(false);
        }
      });
  }

  createMessage(msgData: MessageData) {
    return this.http.post('http://localhost:8000/messages', msgData);
  }
}
