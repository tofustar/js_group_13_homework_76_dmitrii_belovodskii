const express = require('express');
const db = require('../fileDb');

const router = express.Router();

router.get('/', (req, res) => {

  const date = req.query.datetime;
  const datetime = new Date(req.params.datetime);

  if (date){
    if (isNaN(datetime.getDate())){
      let index = db.getMsg().findIndex(message => message.datetime === date);
      let messageArr = db.getMsg().slice(index + 1, db.getMsg().length);
      res.send(messageArr);
    } else {
      res.status(400).send('Date is incorrect');
    }
  } else {
    let messageArr = db.getMsg().slice(db.getMsg().length-30, db.getMsg().length);
    res.send(messageArr);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const message = {
      author: req.body.author,
      message: req.body.message,
    };

    if(!message.author || !message.message) {
      return res.status(400).send({error: 'Author and message must be present in request'});
    }

    await db.addMsg(message);

    return res.send({message: 'Created new product with id: ' + message.id + '. ' +
        + 'And datetime: ' + message.datetime});
  } catch (e) {
    console.error('Error! ' + e);
  }
});

module.exports = router;